//comentar
/*
faça um programa que receba quatro numeros inteiros,
calcule e mostre a soam desses numeros
*/

//primeiro bloco
//inicio
namespace exercicio_1
{
    //entrada de dados
    //var---let---const
    let numero1, numero2, numero3, numero4: number;
    
    numero1 = 6;
    numero2 = 7;
    numero3 = 16;
    numero4 = 34;

    let resultado: number;

    //processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //saida
    console.log("o resultado e" + resultado);
    //ou pode-se
    console.log(`o resultado da soma e ${resultado}`);
}